<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stockmanagement_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    function filled_get_all($table,$data){
    	//$result = $this->db->select('*')->order_by('id','desc')->get_where($table,$data);

        $result = $this->db->query("select id,cylinder,createdon from stock where id not in ( select id from delivery) order by stock.id");
        #echo $this->db->last_query();die();

    	if($result){
    		return $result->result();
    	}
    	else{
    		return false;
    	}
    }
    function empty_get_all($table,$data){
        //$result = $this->db->select('*')->order_by('id','desc')->get_where($table,$data);
        #echo $this->db->last_query();die();
        $result = $this->db->query("select stock.cylinder,delivery.in_date as createdon  from stock inner join delivery on delivery.cylinder = stock.id order by createdon desc");
        if($result){
            return $result->result();
        }
        else{
            return false;
        }
    }
	function insert($table,$data)
	{		
		$this->db->insert($table,$data);
		return true;
	}
    function delete_stock($id){
         $this->db->where('id', $id);
        $this->db->delete('stock'); 
    }
    public function check_stock_duplicate($table,$primaryid){
        $result = $this->db->select('*')->get_where($table,array('cylinder'=>$primaryid));
        //echo $this->db->last_query();die();
        if($result){
            return $result->row();
        }
        else
            return false;
    }
	
}