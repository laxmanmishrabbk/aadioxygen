<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    function stock_cylinder(){

        
        $result = $this->db->query('select stock.id,stock.cylinder from stock where status = 1 and id Not In (select cylinder from delivery)');
    	if($result){
    		return $result->result();
    	}
    	else{
    		return false;
    	}

    }
	function customer_cylinder($customer_id)
	{		
		$result = $this->db->query('select stock.id,stock.cylinder from stock where status = 1 and id In (select cylinder from delivery where returnstatus is null and customer = '.$customer_id.')');
        if($result){
            return $result->result();
        }
        else{
            return false;
        }
	}
    public function outprocess($data,$customer_data,$cyliender_issued){
            
            $result = $this->db->where(array('customer'=>$customer_data,'cylinder'=>$cyliender_issued,))->update('delivery',$data);
            return true;

    }
	
}