<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	public function index()
	{
		if (get_user_info()) {
		$this->load->view('dashboard');
		}
		else{
			redirect('login');
		}
	}
}
