<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {

		parent::__construct();
		$this->load->model('User_model','u');

	}
	public function login()
	{
		if($this->input->post()) {
			$authenticated = $this->u->login_user();
			
			if(!$authenticated) {
				$this->session->set_flashdata('error', 'Invalid login!!!');
				redirect('login');
			} else {
				redirect('dashboard');
			}
		}
		$this->load->view('login');
	}
	
	public function logout() 
	{
		
		unset($_SESSION['userinfo']);
		unset($_SESSION['logged_in']);
		$this->session->set_flashdata('notice', 'You have been logged out successfully!!!');
		redirect('login');
	}

}
