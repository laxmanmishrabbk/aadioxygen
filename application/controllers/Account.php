<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	public function __construct() {
        parent::__construct();
       	$this->table = 'customer';
        $this->load->model('account_model');
    }
	public function index(){
		$this->load->view('account');
	}
	public function debit_get_all(){
		$rowst = array();
		$result = $this->account_model->get_all_debit();
		// echo $this->db->last_query();die();
		// exit;
		foreach ($result as $key => $data) {
			# code...
			$row  = array();
			$row[] = $data->name;
			$row[] = $data->contactno;
			array_push($rowst, $row);
		}
		$json_data = array( "data" => $rowst);
		echo json_encode($json_data);


	}

}