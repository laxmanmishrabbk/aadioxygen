<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
       $this->table = 'delivery';
       $this->table = 'delivery';
        $this->load->model('common_model');
        $this->load->model('delivery_model');
    }
	public function index()
	{
		$this->data['customer_data'] = $this->common_model->get_all('customer',array('status' => 1, ));

		$this->data['cylinder_data'] = $this->delivery_model->stock_cylinder('stock',array('status' => 1, ));

		$this->load->view('new-delivery',$this->data);
	}
	public function insert(){
		
		$customer_data = $this->input->post('customer_data');
		$cyliender_issued = $this->input->post('cyliender_issued');
		$balance_amount = $this->input->post('balance_amount');
		$payment_mode = $this->input->post('payment_mode');
		
		$data = array(
			'customer'=>$customer_data,
			'cylinder'=>$cyliender_issued,
			'out_date'=>date('Y-m-d H:m:s'),
			'pay_mode'=>$payment_mode,
			'refill_amount'=>$balance_amount,
			'createdon'=>date('Y-m-d H:m:s'),
			'status'=>1,
			'deleted'=>0
		);
		$result = $this->common_model->insert($this->table,$data);
		if($result){
			redirect('delivery');
		}
	}
	public function in()
	{
		$this->data['customer_data'] = $this->common_model->get_all('customer',array('status' => 1, ));
		$this->load->view('return-delivery',$this->data);
	}
	public function get_cyliender_data(){

			$customer_id = $this->input->get('customer_id');

			$customer_data = $this->delivery_model->customer_cylinder($customer_id);
			$rowst = array();

			if($customer_data){
				foreach ($customer_data as $key => $data) {
				# code...
					$row = (object)array(); 
					$row->id = $data->id;
					$row->cylinder = $data->cylinder;
					array_push($rowst, $row);
				}
			}

			echo json_encode($rowst);
			
	}
	public function get_balance_amount(){

		$get_customer_data = $this->input->get('customer_id');
		$get_cylinder_data = $this->input->get('cyliender_data');

		$data = array(
			'customer' => $get_customer_data, 
			'cylinder' => $get_cylinder_data, 
		);
		$result = $this->common_model->get_all('delivery',$data);

		$now = time(); // or your date as well
		$your_date = strtotime($result[0]->out_date);
		$datediff = $now - $your_date;

		$totaldays =  round($datediff / (60 * 60 * 24));

		if($totaldays>7){
			$latecharge = ($totaldays -7)*10;
		}
		else{
			$latecharge = 0;
		}
		echo $latecharge;

	}
	public function outprocess(){

		$customer_data = $this->input->post('customer_data');
		$cyliender_issued = $this->input->post('cyliender_issued');
		$balance_amount = $this->input->post('balance_amount');
		$payment_mode = $this->input->post('payment_mode');
		

		$data = array(
			'customer' => $customer_data, 
			'cylinder' => $cyliender_issued, 
		);
		$result = $this->common_model->get_all('delivery',$data);

		if($result[0]->pay_mode == "credit"){
			if($payment_mode == "credit")
			$final_amount = $balance_amount+$result[0]->refill_amount;
			else
			$final_amount = $result[0]->refill_amount;
		}else{
			$final_amount = 0;
		}

		$data = array(
			'in_date'=>date('Y-m-d H:m:s'),
			'latecharge_amount'=>$balance_amount,
			'in_payment_mode'=>$payment_mode,
			'updatedon'=>date('Y-m-d H:m:s'),
			'final_amount'=>$final_amount,
			'returnstatus'=>1
		);
		$result = $this->delivery_model->outprocess($data,$customer_data,$cyliender_issued);


		if($result){
			redirect('delivery/in');
		}
	}
}
