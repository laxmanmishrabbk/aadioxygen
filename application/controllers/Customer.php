<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
       	$this->table = 'customer';
        $this->load->model('common_model');
    }
	public function index()
	{
		$this->data['cylinder_data'] = $this->common_model->get_all('stock',array('status' => 1, ));
		$this->load->view('customer', $this->data);
	}
	public function get_all(){
		
		$rowst = array();
		$data = array(
			'status'=>1,
			'deleted'=>0
		);
		$result = $this->common_model->get_all($this->table,$data);
		// echo $this->db->last_query();die();
		// exit;
		foreach ($result as $key => $data) {
			# code...
			$row  = array();
			$row[] = $data->company;
			$row[] = $data->name;
			$row[] = $data->contactno;
			$row[] = $data->address;
			$row[] = $data->cylinder;
			$row[] = '';
			$row[] = $data->deposit;
			$row[] = "<a href='#' class='edit-customer btn btn-default' data-userid='$data->id' data-company='$data->company' data-name='$data->name' data-contactno='$data->contactno' data-address='$data->address' data-cylinder='$data->cylinder' data-deposit='$data->deposit' ><i class='fa fa-edit'></i></a>&nbsp;&nbsp;&nbsp;<a href='".base_url('customer/delete/'.$data->id)."' class='confirm'><i class='fa fa-trash-o'></i></a>";
			array_push($rowst, $row);
		}
		$json_data = array( "data" => $rowst);
		echo json_encode($json_data);

	}
	public function insert(){
		
		$company_name = $this->input->post('company_name');
		$company_person = $this->input->post('company_person');
		$contact_no = $this->input->post('contact_no');
		$address = $this->input->post('address');
		$cyliender_issued = $this->input->post('cyliender_issued');
		$security_deposit = $this->input->post('security_deposit');
		
		$data = array(
			'company'=>$company_name,
			'name'=>$company_person,
			'contactno'=>$contact_no,
			'address'=>$address,
			'cylinder'=>$cyliender_issued,
			'deposit'=>$security_deposit,
			'status'=>1,
			'deleted'=>0
		);
		if($this->input->post('userid'))
		$result = $this->common_model->update($this->table,$data, array('id'=>$this->input->post('userid')));
		else
		$result = $this->common_model->insert($this->table,$data);

		if($result){
			redirect('customer');
		}
	}
	public function delete($id){

		 $result = $this->common_model->update($this->table, array('status'=>0, 'deleted'=>1), array('id'=>$id));
		 redirect('customer');

	}
	
}
