<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stockmanagement extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
       $this->table = 'stock';
        $this->load->model('stockmanagement_model');
        $this->load->model('common_model');
    }
	public function index()
	{
		$this->load->view('stockmanagement');
	}
	public function filled_get_all(){
		
		$rowst = array();

		$data = array(
			'status'=>1,
			'deleted'=>0
		);
		$result = $this->stockmanagement_model->filled_get_all($this->table,$data);
		foreach ($result as $key => $data) {
			# code...
			$row  = array();
			$row[] = $data->createdon;
			$row[] = $data->cylinder;
			$row[] = '<a href='.base_url('stockmanagement/delete/'.$data->id).' class="confirm"><i class="fa fa-trash-o"></i></a>';
			array_push($rowst, $row);
		}
		$json_data = array( "data" => $rowst);
		echo json_encode($json_data);

	}
	public function empty_get_all(){
		
		$rowst = array();

		$data = array(
			'status'=>1,
			'deleted'=>0
		);
		$result = $this->stockmanagement_model->empty_get_all($this->table,$data);
		foreach ($result as $key => $data) {
			# code...
			$row  = array();
			$row[] = $data->createdon;
			$row[] = $data->cylinder;
			array_push($rowst, $row);
		}
		$json_data = array( "data" => $rowst);
		echo json_encode($json_data);

	}
	public function insert(){
		
		$cylinderno = $this->input->post('cylinderno');
		
		$data = array(
			'cylinder'=>$cylinderno,
			'status'=>1,
			'deleted'=>0
		);
		$result = $this->stockmanagement_model->insert($this->table,$data);
		if($result){
			redirect('stockmanagement');
		}
	}
	public function check_stock_duplicate(){

		$cylinderno = $this->input->get('cylinderno');

		$isAvailable = true; // or false

		$checkstock = $this->stockmanagement_model->check_stock_duplicate('stock',$cylinderno);

		if($checkstock){
			$isAvailable = false; // or false
		}
		// Finally, return a JSON
		echo json_encode(array(
		    'valid' => $isAvailable,
		));
	}
	public function delete($id){
		 $result = $this->stockmanagement_model->delete_stock($id);
		 redirect('stockmanagement');
	}
	public function empty_stock(){
		$this->load->view('empty-stockmanagement');
	}
}
