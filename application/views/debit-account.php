<?php include_once('header.php'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Account</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Debit
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="debitAccountTable">
                                <thead>
                                    <tr>
                                        <th>Customer Name</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                            </table>                
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
    
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- Modal -->
       <!-- /#wrapper -->
<?php include_once('footer.php'); ?>