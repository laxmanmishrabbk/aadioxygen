<?php include_once('header.php'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Customer</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Customer List 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="customerTable">
                                <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Contact Person</th>
                                        <th>Contact Number</th>
                                        <th>Address</th>
                                        <th>Cylinder Issued</th>
                                        <th>Balance Amount</th>
                                        <th>Security Deposit</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>                
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
    
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
<!-- Modal -->
<div class="modal fade" id="customerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="customer/insert" id="customerForm" role="form" method="post">
        <input type="hidden" name="userid">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Customer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group">
                <label>Business Name</label>
                <input type="text" name="company_name" class="form-control" placeholder="Enter Business" required>
            </div>
            <div class="form-group">
                <label>Contact Person</label>
                <input type="text" name="company_person" class="form-control" placeholder="Enter Name" required>
            </div>
            <div class="form-group">
                <label>Contact No.</label>
                <input type="text" name="contact_no" class="form-control" placeholder="Enter Contact No." required>
            </div>
            <div class="form-group">
                <label>Address</label>
                <input type="text" name="address" class="form-control" placeholder="Enter Address" required>
            </div>
            <div class="form-group">
                <label># Cylinder Issued</label>
                <?php
                    $options = array(''=>'Select');
                    $cyliender_issued = array(1,2,3,4,5,6,7,8);
                    foreach ($cyliender_issued as $key => $data) {
                        # code...
                        $options[$data] = $data; 
                    }
                    echo form_dropdown('cyliender_issued', $options,'',array('class'=>'form-control','required'=>true));
                ?>
            </div>
            <div class="form-group">
                <label>Security Deposit</label>
                <input name="security_deposit" class="form-control" placeholder="Enter Amount">
            </div>
            
            <div class="form-group">
                <label>ID Proof</label>
                <input name="id_proof" type="file">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary submit-button">Add Customer</button>
      </div>
     </form>
    </div>
  </div>
</div>
       <!-- /#wrapper -->
<?php include_once('footer.php'); ?>