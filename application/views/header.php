<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>AS Oxygen</title>
    <?php
    //Bootstrap Core CSS
    echo link_tag('vendor/bootstrap/css/bootstrap.min.css');

    //MetisMenu CSS
    echo link_tag('vendor/metisMenu/metisMenu.min.css');

    //Custom CSS
    echo link_tag('dist/css/sb-admin-2.css');
    //Custom Fonts
    echo link_tag('vendor/font-awesome/css/font-awesome.min.css');

    ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url('dashboard'); ?>">Aadishakti Oxygen</a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right">
                Hello : <?php echo get_user_info()->email; ?>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url('logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                             <a href="#"><i class="fa fa-wrench fa-fw"></i> Delivery<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('delivery'); ?>">New Delivery</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('delivery/in'); ?>"> Return</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo base_url('customer'); ?>"><i class="fa fa-edit fa-fw"></i> Customer</a>
                        </li>
                        
                         <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Stock Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('stockmanagement'); ?>">Filled Stock</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('stockmanagement/empty_stock'); ?>"> Empty Stock</a>
                                </li>
                            </ul>
                        </li>
                        <!--li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Account <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php #echo base_url('account'); ?>">Debit</a>
                                </li>
                                <li>
                                    <a href="<?php #echo base_url('account/credit'); ?>"> Credit</a>
                                </li>
                            </ul>
                        </li-->

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>