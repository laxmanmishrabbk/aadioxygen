<?php include_once('header.php'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"> Stock Management</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             Filled
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="stockTable">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Cylinder Number</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>                               
                            </table>
                
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
    
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
     <!-- /#wrapper -->
     <div class="modal fade" id="stockModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<form id="addStockForm"  action="stockmanagement/insert" role="form" method="post" autocomplete="off">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLongTitle">Add Cylinder</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
				<div class="form-group">
					<label>Cylinder Number</label>
					<input type="text" name="cylinderno" class="form-control" placeholder="Cylinder Number" autocomplete="off" required>
				</div>		
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">Add Stock</button>
		  </div>
		</form>
    </div>
  </div>
</div>

<?php include_once('footer.php'); ?>