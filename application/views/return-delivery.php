<?php include_once('header.php'); ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Delivery</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Return Delivery
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <form action="outprocess" id="returndeliveryForm" role="form" method="post">
                                 <div class="form-group">
                                        <label>Date</label>
                                        <input type="text" class="form-control" value="<?php echo date('Y-m-d H:m:s'); ?>" readonly="true">
                                </div>
                                 <div class="form-group">
                                    <label>Customer</label>
                                        <?php
                                            $options = array(''=>'Select Customer');
                                            foreach ($customer_data as $key => $data) {
                                                # code...
                                                $options[$data->id] = $data->name; 
                                            }
                                            echo form_dropdown('customer_data', $options,'',array('class'=>'form-control get_cyliender_data','required'=>true));
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Cylinder</label>
                                        <?php
                                            $options = array(''=>'Select Cylinder');
                                            echo form_dropdown('cyliender_issued', $options,'',array('class'=>'form-control','id'=>'get_cyliender_data','required'=>true,"onchange"=>'get_balance_amount()'));
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Payment Mode</label>
                                        <select name="payment_mode" class="form-control" required>
                                            <option>Select Mode</option>
                                            <option value="cash">Cash</option>
                                            <!--option value="credit">Credit</option-->
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Amount</label>
                                        <input type="text" name="balance_amount" class="form-control" placeholder="Amount" id="balanaceAmount" required="" readonly="">
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-success" type="submit">Submit</button>
                                    </div>
                            </form>                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
    
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
      <!-- /#wrapper -->
<?php include_once('footer.php'); ?>