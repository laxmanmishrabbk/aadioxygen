-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2018 at 05:49 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aadishaktioxygen`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `admin` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `contactno` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `cylinder` int(11) NOT NULL,
  `deposit` float NOT NULL,
  `createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `company`, `name`, `contactno`, `address`, `cylinder`, `deposit`, `createdon`, `status`, `deleted`) VALUES
(1, 'AS Fabrication', 'Manoj', '7000716629', 'Bemetara,kobiya', 3, 10000, '2018-10-20 11:58:41', 1, 0),
(2, 'AS Google LLC', 'Michal Jackson', '9999999999', 'Bemetara', 1, 5000, '2018-10-20 12:01:24', 1, 0),
(3, 'Damroo LLC', 'Damroo', '9999777788', 'mohammedpur khala', 4, 10000, '2018-10-20 20:52:58', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `delivery`
--

CREATE TABLE `delivery` (
  `id` int(11) NOT NULL,
  `customer` int(11) NOT NULL,
  `cylinder` int(11) NOT NULL,
  `out_date` datetime NOT NULL,
  `pay_mode` varchar(255) NOT NULL,
  `refill_amount` int(11) DEFAULT NULL,
  `in_date` datetime DEFAULT NULL,
  `in_payment_mode` varchar(255) DEFAULT NULL,
  `latecharge_amount` int(11) DEFAULT NULL,
  `deliveryby` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedon` datetime DEFAULT CURRENT_TIMESTAMP,
  `final_amount` int(11) DEFAULT NULL,
  `returnstatus` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery`
--

INSERT INTO `delivery` (`id`, `customer`, `cylinder`, `out_date`, `pay_mode`, `refill_amount`, `in_date`, `in_payment_mode`, `latecharge_amount`, `deliveryby`, `createdon`, `updatedon`, `final_amount`, `returnstatus`, `status`, `deleted`) VALUES
(3, 3, 1, '2018-10-20 20:10:03', 'credit', 300, '2018-11-20 19:11:39', 'credit', 240, NULL, '2018-10-20 20:10:03', '2018-11-20 19:11:39', 540, 1, 1, 0),
(4, 3, 2, '2018-11-20 19:11:31', 'credit', 300, '2018-11-20 19:11:42', 'credit', 0, NULL, '2018-11-20 19:11:31', '2018-11-20 19:11:42', 300, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `cylinder` varchar(255) NOT NULL,
  `createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `cylinder`, `createdon`, `status`, `deleted`) VALUES
(3, '12345', '2018-11-21 01:41:34', 1, 0),
(4, '13124125', '2018-11-21 01:41:45', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery`
--
ALTER TABLE `delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `delivery`
--
ALTER TABLE `delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
