  $(document).ready(function() {
        var base_url = $('input[name=base_url]').val();
        $('#dataTables-example').DataTable({
            responsive: true
        });
        var customerTable = $('#customerTable').DataTable({
            responsive: true,
            "ajax": base_url+'customer/get_all',
            initComplete: function(){
                 $('<a id="download" class="btn add-customer" style="margin:0px 2px; padding: 0px 8px; top:-2px;"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Add</a>').appendTo('div#customerTable_filter');
           }    
        });
        $('body').on('click','.add-customer', function () {
            $('#customerModal input[name=userid]').val('');
            $('#customerModal input[name=company_name]').val('');
            $('#customerModal input[name=company_person]').val('');
            $('#customerModal input[name=contact_no]').val('');
            $('#customerModal input[name=address]').val('');
            $('#customerModal select[name=cyliender_issued]').val('');
            $('#customerModal input[name=security_deposit]').val('');
            $('#customerModal .modal-title').text('Add Customer');
            $('#customerModal .submit-button').text('Add Customer');
            $('#customerModal').modal('show');
        });
        customerTable.on('click','.edit-customer', function () {
            var userid = $(this).data('userid');
            var company=$(this).data('company');
            var name=$(this).data('name');
            var contactno=$(this).data('contactno');
            var address=$(this).data('address');
            var cylinder=$(this).data('cylinder');
            var deposit=$(this).data('deposit');

            $('#customerModal input[name=userid]').val(userid);
            $('#customerModal input[name=company_name]').val(company);
            $('#customerModal input[name=company_person]').val(name);
            $('#customerModal input[name=contact_no]').val(contactno);
            $('#customerModal input[name=address]').val(address);
            $('#customerModal select[name=cyliender_issued]').val(cylinder);
            $('#customerModal input[name=security_deposit]').val(deposit);

            $('#customerModal .modal-title').text('Edit Customer');
            $('#customerModal .submit-button').text('Edit Customer');
            $('#customerModal').modal('show');
        })

        customerTable.on('click','.confirm', function () {
            return confirm('Are you sure want to continue?');
        });
        
        var stockTable = $('#stockTable').DataTable({
            sorting:false,
            responsive: true,
            "ajax": base_url+'stockmanagement/filled_get_all',
            initComplete: function(){
                 $('<a id="download" class="btn" style="margin:0px 2px; padding: 0px 8px; top:-2px;"  data-toggle="modal" data-target="#stockModal"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Add</a>').appendTo('div#stockTable_filter');
           }    
        });
        stockTable.on('click','.confirm', function () {
            return confirm('Are you sure want to continue?');
        });
        $('#emptyStockTable').DataTable({
            sorting:false,
            responsive: true,
            "ajax": base_url+'stockmanagement/empty_get_all',
        });


        $('#debitAccountTable').DataTable({
            sorting:false,
            responsive: true,
            "ajax": base_url+'account/debit_get_all',
        });


        


    $('#addStockForm').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            cylinderno: {
                validators: {
                    notEmpty: {
                        message: '* required'
                    },
                    stringLength: {
                        min: 3,
                        max: 30,
                        message: ''
                    },
                    regexp: {
                        regexp: /^[0-9_]+$/,
                        message: 'Enter Cylinder Number only'
                    },
                    remote: {
                        message: 'Cylinder no. already exists ',
                        url: base_url+'stockmanagement/check_stock_duplicate'
                    }
                }
            },
        }
    });
    $('form#customerForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            company_name: {
                validators: {
                    notEmpty: {
                        message: '* required'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'must be more than 6 and less than 30 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9\s]+$/,
                        message: 'only consist of alphabetical, number and underscore'

                    }
                }
            },
            contact_no:{
                validators: {
                notEmpty: {
                        message: '* required'
                },
                regexp: {
                        regexp: /^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$/,
                        message: 'invalid format'

                    }
                }
            },
            security_deposit:{
                validators: {
                    notEmpty: {
                            message: '* required'
                    },
                    regexp: {
                        regexp: /^[1-9]\d*(\.\d+)?$/,
                        message: 'invalid format'

                    }
                }
            }
        }
    });

    $('form#newdeliveryForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            balance_amount: {
                validators: {
                    notEmpty: {
                        message: '* required'
                    },
                    regexp: {
                        regexp: /^[1-9]\d*(\.\d+)?$/,
                        message: 'invalid format'

                    }
                }
            },
        }
    });

    $('.get_cyliender_data').change(function(){

        var get_customer = $(this).val();
        $('#get_cyliender_data').html('<option>Select Cylinder</option>');
        $.getJSON('get_cyliender_data', { customer_id : get_customer },function (data) {
            $.each(data, function (index, value) {
                // APPEND OR INSERT DATA TO SELECT ELEMENT.
                $('#get_cyliender_data').append('<option value="' + value.id + '">' + value.cylinder + '</option>');
            });
        });
    });  

        
        // var get_customer = $(this).val();
        // $('#get_cyliender_data').html('<option>Select Cylinder</option>');
        // $.getJSON('get_cyliender_data', { customer_id : get_customer },function (data) {
        //     $.each(data, function (index, value) {
        //         // APPEND OR INSERT DATA TO SELECT ELEMENT.
        //         $('#get_cyliender_data').append('<option value="' + value.id + '">' + value.cylinder + '</option>');
        //     });
        // });
    });   

     function get_balance_amount(){
        var get_customer_data = $('select[name="customer_data"]').val();
        var get_cyliender_data = $('select[name="cyliender_issued"]').val();
        $.get( "get_balance_amount",{ customer_id : get_customer_data, cyliender_data : get_cyliender_data }, function( data ) {
              $('#balanaceAmount').val(data);
        });

    }